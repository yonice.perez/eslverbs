<?php
class Building_Model extends APP_Model{
	
    private $fields_info = [ 'building.id',
                    'building.title',
                    "DATE_FORMAT( building.date_send, '%d - %m - %Y') as date_send", 
                    'building.status',
                    'building.user_applicant_id',
                    'user_applicant.email as email',
                    'concat(user_applicant.name," ",user_applicant.surname_father) as nombres '];

	public function record_count( $where = null ) 
    {
        $this->search( $where );
        $this->db->from('building')
            ->join('user_applicant', 'building.user_applicant_id = user_applicant.id');
        return $this->db->count_all_results();
    }

    public function get_data( $limit,  $start, $where = null ) 
    {

        $this->db->select( implode(',', $this->fields_info) )	
        	->from('building')
        	->join('user_applicant', 'building.user_applicant_id = user_applicant.id')
        	->order_by('building.date_create DESC')
        	->limit($limit, $start);

        $this->search( $where );

        return $this->db->get()->result();
    }
    
    private function search( $where )
    {
		if(isset($where['title']))
        {
        	$this->db->like('title', $where['title'] );
        }
        if(isset($where['email']))
        {
        	$this->db->like('email', $where['email'] );
        }
        if(isset($where['nombres']))
        {   
        	$this->db->like('concat(user_applicant.name," ",user_applicant.surname_father)', $where['nombres'] );
        }
        if(isset($where['date_send']))
        {       
        	$this->db->where("DATE_FORMAT(  building.date_send, '%d-%m-%Y' ) = ", $where['date_send'] );
        }
        if(isset($where['status']))
        {	
        	$this->db->where('building.status', $where['status'] );
        }
    }

    public function getInfo( $id )
    {
            
        $id = (int)$id;

        $this->db->select( implode(',', $this->fields_info ) ) 
            ->from('building')
            ->join('user_applicant', 'building.user_applicant_id = user_applicant.id')
            ->where('building.id' , $id );

        return $this->db->get()->row();
    } 
}