<?php
/**
 * @route::verbs
 */
class Verbs extends CI_Controller {
		
	public function zh()
	{	
		$this->template->theme('public');	
		$this->template->js('verbs.zh');
		$this->template->view('view-app-list');
	}

	public function index()
	{	
		$this->template->theme('public');	
		$this->template->js('verbs.index');
		$this->template->view('view-app-list');
	}

	/**
	 * @route::data
	 * @route::data/(:any)
	 */
	public function data( $lan = '')
	{	

		$file_regular   = file(FCPATH . 'resources/_default/verbs.csv');
		//$file_irregular = file(FCPATH . 'resources/_default/verbs.irregular.txt');	
		$data           = $file_regular;
		sort($data);
		$input          = '';	
		if($input = $this->input->get('search'))
		{		
			$input  = strtolower($input);
				
			$data = array_filter($data, function ($item) use ($input) {
			    if (stripos( strtolower($item), $input) !== false) {
			        return true;
			    }
			    return false;
			});
		}

		$data = array_map(function(  $v ) use( $input , $lan){
			if($input!='')
			{	
				$v = str_replace($input, '<u class="text-warning" style="font-weight:bold;">'.$input.'</u>', strtolower( $v ) );
			}

			$aux        =  explode('|', $v);

			if( count($aux)!=8){
				return false;
			} 	

			$new_result['word']               =  strtolower( $aux[0] );
			$new_result['future']             =  strtolower( $aux[1] );
			$new_result['present_continuous'] =  strtolower( $aux[2] );
			$new_result['past']               =  strtolower( $aux[3] );
			$new_result['third_person']       =  strtolower( $aux[4] );
			$new_result['past_participle']    =  strtolower( $aux[5] );
			$new_result['translate']          =  ($lan =='zh') ? $aux[7] : mb_strtolower($aux[6], 'UTF-8');	
				
			return $new_result;		
		},  $data );
		//
		if($input=='')
		{	
			shuffle($data);
		}
		//
		$this->load->view('view-app-data', ['verbs' => $data]);
			
	}

}
