<?php 
/**
 * Template
 */
class Template {

    protected $CI;
    
    protected $session_error       = 'msg_error';
    
    protected $session_success     = 'msg_success';
    
    protected $include_js          = array();
    
    protected $include_css         = array();
    
    protected $include_modal       = array();
    
    protected $include_js_external = array();
    
    protected $theme_selected      = '';
    
	function __construct( $config = array() ){
		$this->CI =& get_instance();
        /**
         * default js
         */  
        foreach ($config['default_js'] as $value) {
            $this->include_js[] = $value;
        } 
        /**
         * default css
         */
        foreach ($config['default_css'] as $value) {
            $this->include_css[] = $value;
        } 
        
    }       

    /**
     *->modal
     *  
     *  Define view type modal in view/themes/_modal-header.php
     *
     * @param view   string     name file view
     * @param id     string     name id html open modal
     * @param config array      config init modal 
     */
    function modal( $view, $id , $config = null) {
            
        $config['size']   = (isset($config['size']))    ? $config['size'] : 'modal-lg';
        $config['header'] = (isset($config['header']) )   ? $config['header'] : '';
        
        $this->include_modal[]  = ['view' => $view, 'id' => $id ,'config' => $config];
        return $this;
    }

    /**
     *->theme
     *  
     *  Define view header and footer theme in folder view/themes/
     *
     * @param name   string     name file view
     */
    function theme( $name ){    
        $this->theme_selected = $name;
    	return $this;
    }

    /**
     *->view [render website]
     *  
     *  Define view default
     *
     * @param view   string     name file view
     * @param parms  array      vars default view
     * @param return boolean    return html or print
     */
    function view($view, $parms = array(), $return = FALSE ){
        $vars_header['content']['messages'] = $this->session_messages();

        
        $css = ''; 
        foreach ($this->include_css as $value) {
            $css.= $this->CI->resource->css( $value['file'],  $value['path'], FALSE );   
        }
                
        $vars_header['include_css'] = $css;
            
        if($return) 
        {      
            $content = $this->CI->load->view('_themes/'.$this->theme_selected.'-header', $vars_header, TRUE);
            $content.= $this->CI->load->view($view, ['_vars' => $parms], TRUE);
            $content.= $this->CI->load->view('_themes/'.$this->theme_selected.'-footer', null, TRUE);
            return $content;
        }           
        else
        {
            

            $this->CI->load->view('_themes/'.$this->theme_selected.'-header', $vars_header, FALSE);
            /**
             * lightboxs
             */ 
            foreach ($this->include_modal as $modal) {
                $modal['id'] = (isset($modal['id'])) ?  $modal['id'] : uniqid();
                $this->CI->load->view('_themes/_modal-header', ['modal' => $modal], FALSE);
                $this->CI->load->view($modal['view'],  ['modal' => $modal] , FALSE); 
                $this->CI->load->view('_themes/_modal-footer', null, FALSE); 
            }

            $this->CI->load->view($view, ['_vars' => $parms], FALSE);
            
            $js = '';
            foreach ($this->include_js as $value) { 
                $js.= $this->CI->resource->js( $value['file'],  $value['path'], false );   
            }
            
            $this->CI->load->view('_themes/'.$this->theme_selected.'-footer', ['parms' => ['js' => $js ]], FALSE);
        }
    }

    /**
     *->error
     *  
     *  Define message error, error set in session_flash
     *
     * @param message_error   string|array      message destroy after redirect
     */
    function error( $message_error ){
        $msg = '';
    	if(is_array($message_error)){
            foreach ( $message_error as $key => $value) 
            {
                $msg.= "<p>".$value."</p>";
            }
    	}else{ 
			$msg = $message_error;    		
    	}

        $this->CI->session->set_flashdata(  $this->session_error , $msg ); 
        return $this;
    }

    /**
     *->success
     *  
     *  Define message success, success set in session_flash
     *
     * @param msg   string|array      message destroy after redirect
     */ 
    function success( $msg ){
        $this->CI->session->set_flashdata ( $this->session_success , $msg );
        return $this;
    }
    
    /**
     *->json
     *  
     *  Send json output config @ReturnJson in comments
     *  
     * @param data   array|object      send json output
     */
    function json( $data ){

        echo json_encode( $data );
    }


    /**
     *->js
     *  
     *  Set JS end document
     *  
     * @param file   string      name file config in /resources/js/
     * @param path   string      path file changed for external if file=''
     */
    function js( $file, $path = ''){
        $this->include_js[] = [ 'file' => $file, 'path' => $path  ];
        return $this;
    }

    /**
     *->js
     *  
     *  Set CSS init document
     *  
     * @param file   string      name file config in /resources/css/
     * @param path   string      path file changed for external if file=''
     */
    function css( $file, $path = ''){
        $this->include_css[] = [ 'file' => $file, 'path' => $path  ];
        return $this;
    }   

    /**
     *->current_user
     *  
     *  get current user and redirect if have url
     *  status 0 = Inactivo
     *  status 1 = Activo
     *  status 2 = Pendiente activar
     *
     * @param redirect   string      full url redirect if not logged
     */
    function current_user( $type_user ='' , $redirect = ''){
        $exit = false;
        
        if(!isset($this->CI->session->userdata[$type_user]->id ) ){
            $exit = true; 
        }else if($user = $this->CI->{$type_user}->getBy('id', $this->CI->session->userdata[$type_user]->id )){
            if( (int) $user->status === 1){
                return $this->CI->session->userdata[$type_user] = $user;    
            }else{
                $exit = true;
            } 
        }else{
            $exit = true;
        }   
        
        if($exit && $redirect!='' ){
            redirect($redirect);
        }else{ 
            return false;
        }
    } 

    /**
     *->load_menu
     *  
     *  File menu to load resources/_default/menu.yml
     *  
     */
    function load_menu( $menu_name = 'internal' ){   
        $this->CI->load->library('Spyc');
        $file           = FCPATH . 'resources/_default/menu-'.$menu_name.'.yml';
        $options_menu   = $this->CI->spyc->YAMLLoad( $file );
        return $options_menu; 
    }       
    
    /**
     *->load_menu
     *  
     *  get menu from load_menu();
     *
     * @param user   data object user
     */
    function get_html_menu( $user = null ){
        $option_menu = $this->load_menu();
        $html_menu   = '<ul class="nav navbar-nav">';



        foreach ($option_menu as $parent_name => $value) {

            if( $user->role == 0 &&  (strpos($user->permits, $parent_name.'::' ) === false) ){
                continue;
            }
            $html_menu.= '<li id="fat-menu" class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                '.$parent_name.' <span class="caret"></span>
                            </a>';
            $html_menu.= '<ul class="dropdown-menu">';
            foreach ($value as $name => $url) { 
                if( $user->role == 0 &&  (strpos($user->permits, $parent_name.'::'.$name ) === false) ){
                    continue;   
                }
                $html_menu.= ' <li><a href="'.site_url($url['default']).'">'.$name.'</a></li>';
            }   
            $html_menu.='</ul></li>';
        }
        $html_menu.= '</ul>';
        return $html_menu;
    }

    /**
     *->load_menu
     *  
     *  get menu from load_menu();
     *
     * @param user   data object user
     */
    function get_html_menu_applicant(){
        $option_menu = $this->load_menu('applicant');
        $html_menu   = '<ul class="nav navbar-nav">';
        
        foreach ($option_menu as $parent_name => $value) {

            $html_menu.= '<li id="fat-menu" class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                                '.$parent_name.' <span class="caret"></span>
                            </a>';
            $html_menu.= '<ul class="dropdown-menu">';
                
            foreach ($value as $name => $url) { 
                $html_menu.= ' <li><a href="'.site_url($url['default']).'">'.$name.'</a></li>';
            }   
            $html_menu.='</ul></li>';
        }
        $html_menu.= '</ul>';
        return $html_menu;
    }
    
    /**
     *->base64_img
     */
    function base64_img( $file ){
        $type     = pathinfo(  $file, PATHINFO_EXTENSION);
        $contents = file_get_contents($file);   
        return 'data:image/' . $type . ';base64,' . base64_encode($contents);
    } 
        
    /**
     *->renderImage
     */
    function render_image( $file ){
        $info   = getimagesize($file);
        header( "Content-type: ".$info['mime'] );
        readfile( $file );  
        exit();
    }

    /**
     *-> ip public
     */
    function ip_public()
    {   
        $url = 'https://api.ipify.org';
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $content = curl_exec($curl);

        curl_close($curl);
        
        //preg_match('/Current IP Address: \[?([:.0-9a-fA-F]+)\]?/', $content, $m);
        return $content;

    }

    /**
     *->session_messages
     *
     * Session flash open if exist
     *
     * @return html [msg_error, msg_success]
     */
    private function session_messages(){
       	
        $msg_error =  $msg_success = '';
        if( $msg = $this->CI->session->flashdata ( $this->session_error ) )
        {   
            $msg_error.= '<div  id="alert-msg-error" class="alert alert-danger" role="alert">';
            $msg_error.=  $msg.'</div>';  

            $msg_error.= '<script>
                    document.addEventListener("DOMContentLoaded", function(event) { 
                        setTimeout(function(){  
                            $("#alert-msg-error").slideToggle(500);
                        },6000);                    
                    });        
                </script>';
        }

        if( $msg = $this->CI->session->flashdata($this->session_success))
        {
            $msg_success.= '<div  id="alert-msg-success" class="alert alert-success" role="alert">';
            $msg_success.= $msg.'</div>';
            $msg_success.= '<script>
                    document.addEventListener("DOMContentLoaded", function(event) { 
                        setTimeout(function(){  
                            $("#alert-msg-success").slideToggle(500);
                        },6000);                
                    });     
                </script>';
        }

        return Array( 
                'msg_error'     => $msg_error, 
                'msg_success'   => $msg_success
            );
    }
}
