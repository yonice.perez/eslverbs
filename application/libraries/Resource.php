<?php
class Resource{
		
	protected $path_js             = '';
	
	protected $path_css            = '';
	
	protected $new_id              = '';
	
	private static $path_resources = 'resources';

	function __construct( $config = array() ){
		$this->path_js  = self::$path_resources.'/js/';
		$this->path_css = self::$path_resources.'/css/';
		$this->new_id   =  '?id='.uniqid();
	}
	
	public function js($file , $path = '', $print = TRUE ){
		$path     = ($path!='') ? $path : $this->path_js;
		if($file==''){	
			$file_url = $path . $this->new_id;
		}else{
			$file_url = base_url($path.$file.'.js'). $this->new_id;
		}	
		if($print){
			echo '<script type="text/javascript"  src="'.$file_url.'"></script>';
		}else{
			return '<script type="text/javascript"  src="'.$file_url.'"></script>';
		}
	}	
	
	public function css($file, $path = '', $print = TRUE){
		$path = ($path!='') ? $path : $this->path_css;
		if($file==''){
			$file_url = $path . $this->new_id;
		}else{
			$file_url = base_url($path.$file.'.css') . $this->new_id; 
		}

		if($print){
			echo '<link rel="stylesheet" href="'.$file_url.'" type="text/css" />';
		}else{
			return '<link rel="stylesheet" href="'.$file_url.'" type="text/css" />';
		}
	}
}