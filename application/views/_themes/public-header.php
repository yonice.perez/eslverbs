<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="Jonathan Q">     
        
        <title>Public</title>
        
        <?php       
            $this->resource->css('/bootstrap.flatly.min', '/resources/vendor/bootstrap' ); 
            $this->resource->css('app_styles'); 
            if(isset($parms['include_css'])){
                echo $parms['include_css'];
            }   
        ?>

        <script type="text/javascript"> 
            $base_url = function( url ){ url = url || ''; return '<?= base_url();?>' + url };
            $site_url = function( url ){ url = url || ''; return '<?= site_url();?>' + url };
        </script>
    </head> 
    <body cz-shortcut-listen="true">
            
        <!-- Static navbar -->
        <div class="container text-center" >         
            <div style="max-width:600px;margin:20px auto;text-align:left;">
                <?php
                    echo $content['messages']['msg_error'];
                    echo $content['messages']['msg_success'];
                ?>  
            </div>
        </div>
        