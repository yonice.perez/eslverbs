<div class="container">
	<div class="form-horizontal well row">
		<div class="form-group">
			<label class="col-md-3"> <a href="<?= site_url();?>">Translate Spanish</a> </label>
			<label class="col-md-6"></label>
			<label class="col-md-3 text-right"> <a href="<?= site_url('/verbs/zh');?>">Translate Chinese</a></label>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label">type a verb</label>
			<div class="col-md-8">
				<input class="form-control" placeholder="(Ej: go) " type="text" id="search" />
			</div>
			
		</div>
	</div>
	
	<div class="row header-list visible-lg">
		<div class="col-lg-2"><b>Word</b></div>
		<div class="col-lg-2"><b>Present Continuous</b></div>
		<div class="col-lg-2"><b>Past</b></div>
		<div class="col-lg-2"><b>Third Person</b></div>
		<div class="col-lg-2"><b>Past Participle</b></div>
		<div class="col-lg-2"><b>Translate</b></div>
	</div>	
	<hr />
	<div id="list-results">
		
	</div>
</div>