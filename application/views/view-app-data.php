<?php 
$aux = 0;

if( count($verbs) === 0 )
{
 	echo '<div class="row alert alert-warning text-center">';
 		echo '<h3>Word not found</h3>';
 	echo '</div>';
	exit;	
}

foreach ($verbs as $verb): $aux++; if($aux == 31) break;?>
	<div class="row alert alert-info">
		<div class="col-lg-2"> 
			<table class="">
				<th class="hidden-lg" style="width:85px;">Word</th>
				<td></td>	
				<td><?= $verb['word']?></td>
			</table>
		</div>
		<div class="col-lg-2">
			<table class="">
				<th class="hidden-lg" style="width:85px;">Present</th>
				<td></td>	
				<td><?= $verb['present_continuous']?></td>
			</table>
		</div>
		<div class="col-lg-2">
			<table class="">
				<th class="hidden-lg" style="width:85px;">Past</th>
				<td></td>	
				<td><?= $verb['past']?></td>
			</table>
		</div>
		<div class="col-lg-2">
			<table class="">
				<th class="hidden-lg" style="width:85px;">3rd. person</th>
				<td></td>	
				<td><?= $verb['third_person']?></td>
			</table>
		</div>
		<div class="col-lg-2">
			<table class="">
				<th class="hidden-lg" style="width:85px;">Past/Part</th>
				<td></td>	
				<td><?= $verb['past_participle']?></td>
			</table>
		</div>
		<div class="col-lg-2">
			<table class="">
				<th class="hidden-lg" style="width:85px;">Translate</th>
				<td></td>	
				<td><?= $verb['translate']?></td>
			</table>
		</div>
	</div>
<?php endforeach; ?>
