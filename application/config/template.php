<?php

/**
 * Default js in order
 */
$config['default_js'] = [
	['file' => '/jquery.min', 'path' =>  '/resources/vendor/jquery' ],
	['file' => '/jquery-ui.min', 'path' =>  '/resources/vendor/jquery' ],
	['file' => '/bootstrap.min', 'path' =>  '/resources/vendor/bootstrap' ],
	['file' => '/bootstrap-datepicker.min', 'path' =>  '/resources/vendor/bootstrap' ],
	['file' => '/bootstrap-datepicker.es', 'path' =>  '/resources/vendor/bootstrap/locale' ],
	['file' => '/angular.min', 'path' =>  '/resources/vendor/angular' ],
	['file' => '/select2.full.min', 'path' => '/resources/vendor/selectjs/js' ],
	['file' => '/_config', 'path' =>  '/resources/js' ],
];
	
	
/**
 * Default css in order
 */		
$config['default_css'] = [
	['file' =>'/bootstrap.flatly.min', 'path' =>  '/resources/vendor/bootstrap' ],
	['file' => '/bootstrap-datepicker.min', 'path' =>  '/resources/vendor/bootstrap' ],
	['file' =>'/font-awesome.min', 'path' =>  '/resources/vendor/font-awesome-4.5.0/css' ],
	['file' => '/select2.min', 'path' => '/resources/vendor/selectjs/css' ],
	['file' =>'checkbox-radio', 'path' =>  '' ],
	['file' =>'app_styles', 'path' =>  '' ]
];	
