<?php
		
$route['default_controller']   = 'verbs/index';
$route['404_override']         = '';
$route['translate_uri_dashes'] = FALSE;

$file_path = APPPATH . 'config/routes-generator.php';

if(file_exists($file_path)){

	include_once $file_path;
	
	if (isset($include_route) && is_array($include_route))
	{	
		$route = array_merge( $route, $include_route);
	}	
}
