<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
|@validate::
| 	UserApplicant       = Usuarios activos en la tabla 'user_applicant.status=1'
| 	UserInternal        = Usuarios activos en la tabla 'user_internal.status=1'
| 	UserInternalAdmin   = Usuarios activos en la tabla 'user_internal.role=1' tipo administrador
| 	UserInternalPermits = Usuarios activos en la tabla 'user_internal.permits array' y con permisos
*/
	
$hook['post_controller_constructor'] = function()
{
	//Obtenemos la instancia
	$_ci=& get_instance();
	//Nombre de la clase
	$class      = $_ci->router->fetch_class();
	//Nombre de la funcion
	//$function = $_ci->router->fetch_method();
	//Datos reflection
	$class_reflection = new ReflectionClass( ucfirst($class) );
	//Documentos en la funcion
	$class_document   = $class_reflection->getDocComment();
	//Filtramos por una cadena que comienze con @validate::contenido_validate
	$pattern = '/@validate::([a-zA-Z0-9\_]+)/';

	if( preg_match_all( $pattern, $class_document, $matches ) > 0 )
	{
		$annotations = $matches[1];	
		
		if(in_array('UserApplicant' , $annotations))
		{
			$user = $_ci->template->current_user('User_Applicant_DB');
			if(!$user || $user->status != 1)
			{
				redirect('/userapplicantpublic/login/'); 
				exit();
			}
			return true;
		}
		else if(in_array('UserInternal' , $annotations))
		{
			$user = $_ci->template->current_user('User_Internal_DB');
			if(!$user || $user->status != 1)
			{
				redirect('/userinternalpublic/login/'); 
				exit();
			}
			return true;
		}
		else if(in_array('UserInternalAdmin' , $annotations))
		{
			$user = $_ci->template->current_user('User_Internal_DB');
			if(!$user || $user->status != 1 || $user->role != 1)
			{
				redirect('/userinternalpublic/login/');
				exit();
			}
			return true;
		}
		else if(in_array('UserInternalPermits' , $annotations))
		{
				
			$valid_permits = function( $ci , $class )
			{			
				$class = strtolower( $class );
				$user  = $ci->template->current_user('User_Internal_DB');
				
				if(!$user) return false;
				if($user->role == 1) return true; 

				$options_menu 	= explode('||', $user->permits );
				//die('<pre>'.print_r($).'</pre>');
				$load_menu 		= $ci->template->load_menu();
				
				foreach ($options_menu as $key => $value) 
				{
					$tmp 	 = explode("::", $value );
					$process = $tmp[0];
					$action  = $tmp[1];

					if( isset( $load_menu[$process][$action]['class'] ) )
					{
						$class_access = $load_menu[$process][$action]['class'];
						if($class_access == $class)
						{
							return true;
						}
					}
				}
				return false;
			};	
			
			if(!$valid_permits( $_ci, $class ))
			{	
				unset($valid_permits);
				redirect('/userinternalpublic/login/');
				exit();
			}
			unset($valid_permits);
			return true;
		}
	}
};