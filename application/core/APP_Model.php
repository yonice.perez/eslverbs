<?php
class APP_Model extends CI_Model {

    /**
    * connection
    */
    protected $db     = null;

    /**
    * data to save or update
    */
    protected $data   = array();

    /**
    * table default is name file
    */
    protected $table   = null;      

    /**
    * id is required all tables
    */  
    protected $primarykey   = 'id';      
    
    function __construct(){
        $ci       =& get_instance();
        $this->db = $ci->db;     
        
        $this->table = (is_null($this->table)) ? 
            strtolower(preg_replace("/_Model\z/i", "", get_class($this) )) : 
            $this->table;
            
        $this->primarykey = (is_null($this->primarykey)) ? 'id' : $this->primarykey;
    }
     
    function __set( $name, $value ){         
        $this->data[$name] = $value;
    }
    
    function __get($name) {  
        return isset($this->data[$name]) ? $this->data[$name] : '';
    }
        
    public function getByArray( $array ){
        $this->db->where( $array );
        return $this->db->get( $this->table )->row();
    }

    public function getBy( $field, $value){
        $this->db->where($field, $value ); 
        return $this->db->get( $this->table )->row();
    }

    public function getFiltered( $filters ){
        $this->db->where( $filters );
        return $this->db->get( $this->table )->result();  
    }
     
    public function save( $id = 0){

        if( $id > 0)
        {
           $this->db->where($this->primarykey , $id )->update($this->table, $this->data );
           return $id;
        }
        else
        {               
           $this->db->insert($this->table, $this->data );
           return $this->db->insert_id();
        }
    }
    
    public function getAll( $filters = null ){
        if(!is_null($filters)){
            $this->db->where( $filters );
        }
        return $this->db->get( $this->table )->result();  
    }

    public function delete( $id = ''){
        $id = (isset($this->data[$this->primarykey])) ? $this->data[$this->primarykey]  : $id;
        return $this->db->delete( $this->table , [$this->primarykey => $id ] );
    }
    
    public function deleteBy( $field , $value = null ){
        $this->db->where( $field, $value ); 
        return $this->db->delete( $this->table );
    }

    public function resetData(){
        $this->data = array();
    }
        
    public function setData( $data ){
        $data = (is_object($data)) ? (Array) $data : $data;
        return $this->data = $data;
    }
}    