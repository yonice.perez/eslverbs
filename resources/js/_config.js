$.importScript = function( url ){	
	var fileref=document.createElement('script');
	fileref.setAttribute("type","text/javascript");
	fileref.setAttribute("src", url );
	document.getElementsByTagName("head")[0].appendChild(fileref);
}
    
var MY_ALERT = null;

$.fn.messageSuccess = function( text, timelimit ){
    var timelimit = timelimit || 3000;
        
    var html = '<div class="alert alert-success alert-dismissible fade in" role="alert" style="padding-bottom:10px;padding-top:10px;" >';
        html+= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        html+= text;
        html+= '</div>';        
        
    

    $( this ).html( html ).css('display','none');
    $( this ).slideDown('slow'); 

    if(MY_ALERT != null)
    {   
        MY_ALERT = clearTimeout(MY_ALERT); 
        MY_ALERT = null;
    }   
    if( timelimit > 0){ 
        MY_ALERT = setTimeout(function(){  
                $('.alert-success').slideUp('slow');
        }, timelimit );
    }
}
    
$.fn.messageError = function( text ){       
        
    var html = '<div class="alert alert-danger alert-dismissible fade in" role="alert" style="padding-bottom:10px;padding-top:10px;" >';
        html+= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        html+= text;
        html+= '</div>';        
       
    $( this ).html( html ).css('display','none');
    $( this ).slideToggle('slow');   
}

$.fn.inner_msg_error   = function( msg ){
    var add_id = 'inner-msg-id-' + new Date().getTime();
    var type   = $(this).attr('type');
    var name   = $(this).attr('name');

    var add_msg  = '<span';
        add_msg += ' id="'+add_id+'"  ';
        add_msg += ' class="help-block text-danger inner-msg"';
        add_msg += ' data-name="' +name +'" ';
        add_msg += ' style="display:none;" >'+msg+'</span>';
    
    $('.inner-msg[data-name='+name+']').remove();
    
    if(type === 'file'){
         $(this).parent().parent().append( add_msg );
    }else if( type === 'radio'){
        $(this).parent().parent().append( add_msg );    
    }else{
        $(this).parent().append( add_msg );
    }   
    
    $( '#' + add_id ).show('fast');
    setTimeout(function(){
        $(  '#' + add_id  ).hide('fast');
    }, 3000);
}

$.fn.inner_msg_success = function( msg ){

    var add_id = 'inner-msg-id-' + new Date().getTime();
    var type   = $(this).attr('type');
    var name   = $(this).attr('name');

    var add_msg  = '<span';
        add_msg += ' id="'+add_id+'"  ';
        add_msg += ' class="help-block text-success inner-msg"';
        add_msg += ' data-name="' +name +'" ';
        add_msg += ' style="display:none;" >'+msg+'</span>';
    
    $('.inner-msg[data-name='+name+']').remove();
    
    if(type === 'file'){
         $(this).parent().parent().append( add_msg );
    }else if( type === 'radio'){
        $(this).parent().parent().append( add_msg );    
    }else{
        $(this).parent().append( add_msg );
    }   

    $( '#' + add_id ).show('fast');
    setTimeout(function(){
        $(  '#' + add_id  ).hide('fast');
    }, 3000);
}
        
$loadingSVG = '<img src="'+$site_url('resources/reload.svg')+'" />';
        
$.fn.setLoading = function( msg ){
    var message = msg || '';
    var columns = $(this).data('colspan');  
    var html  = '<tr><td colspan="'+columns+'" style="text-align:center !important;">'+ $loadingSVG +' '+message +'</td></tr>';
    $(this).html( html );
}
    
$(document).ready(function(){
        
    var select_2 = $("select.create-select2");
    
    if( select_2.length ){
        $( select_2 ).select2();
    }
    
    $('.element-datepicker').datepicker({
        format: 'dd-mm-yyyy',
        language: 'es',
        autoclose: true,
        toggleActive: true
    });
})
