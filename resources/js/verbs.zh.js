$(document).ready(function(){
	filterData();
	
	$('#search').on('keyup', function(){
		filterData();		
	});
});

var filterData = function(){
		
	$.ajax({
		url:$site_url('/verbs/data/zh'),
		data:{ search : $('#search').val() },
		success:function( response ){
			$('#list-results').html( response );
		}
	});
}