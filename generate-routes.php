<?php
/**
 * CLI element
 * buscar los controladores con la cadena @route::nombrederuta
 * @route::welcome/admin/
 * @route::welcome/private/
 * @route::welcome/private/
 */
define('BASEPATH', str_replace('\\', '/', 'system'));
define('APPPATH',  realpath('application') );


$GenerateRoutes = function(){
	
	$pattern_routes = '/@route::([a-zA-Z0-9\/\-\:\(\)\_]+)/';

	require_once BASEPATH.'/core/Controller.php';
	
	$create_route = [];
	$files = new RecursiveIteratorIterator( new RecursiveDirectoryIterator(APPPATH . '/controllers/'));
	foreach ($files as $file)
	{
		$file_info = pathinfo( $file );

		if($file_info['extension']!='php')
		{
			continue;
		}

		require_once $file;	

		$dirname 	= basename( dirname( $file) );
		if( $dirname === 'controllers')
		{
			$filename = $file_info['filename'];
		}else
		{
			$filename = $dirname . '/'. $file_info['filename'];
		}	
		
		$reflection_clas = new ReflectionClass( $file_info['filename'] );
		$docs_class      = $reflection_clas->getDocComment();
		$methods         = $reflection_clas->getMethods(ReflectionMethod::IS_PUBLIC);
		
		
		if(preg_match_all($pattern_routes, $docs_class , $matches_class) > 0 )
		{
			$route_controller = $matches_class[1][0];
		}
		else
		{
			$route_controller = strtolower( $file_info['filename'] );
		}

		foreach ($methods as $reflection) 
		{
			$route_action = strtolower( $reflection->name );
			$name_param   = $reflection->name.'/';

			if ( substr( $reflection->name, 0, 1 ) === '_' ||  $reflection->name === 'get_instance')
			{
				continue;		
			}

			$reflection_method = new ReflectionMethod( $file_info['filename'] ,  $reflection->name );
			$docs_method       = $reflection_method->getDocComment();
			
			$aux_param = array();
			foreach ($reflection_method->getParameters() as $num => $value) 
			{
				$name_param.= '$'. ($num + 1).'/';
			}	

			if (preg_match_all( $pattern_routes , $docs_method, $matches_method ) > 0 ) 
			{		
				$matches_method_tmp = $matches_method[1];
				foreach ($matches_method_tmp as $key => $value) 
				{
					$create_route[ $route_controller. '/' . $value ] = $filename. '/' .$name_param;
				}
			}else
			{
				$create_route[ $route_controller. '/' . $route_action ] = $filename. '/' .$name_param;
			}
		}
	}

	return $create_route;
};



$add_routes_generator = $GenerateRoutes();

ob_start();
echo "<?php \n$"."include_route = array();\n";
foreach ($add_routes_generator as $route => $action ) {	
	echo "$"."include_route"."['".$route."'] = '".$action."';\n";
}
$content = ob_get_clean();

$file_path = APPPATH . '/config/routes-generator.php';
if( $fp = fopen( $file_path, 'w') )
{					
	fputs($fp, $content );
    fclose($fp);
    chmod($file_path,  0765);
}
echo $content;